package com.spiritmark.service;

import com.spiritmark.domain.Product;

public interface ProductService {
    Product findByPid(Integer pid);
}