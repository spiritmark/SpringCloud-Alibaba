package com.spiritmark.service;

import com.spiritmark.domain.Order;

public interface OrderService {

    //创建订单
    void createOrder(Order order);
}
