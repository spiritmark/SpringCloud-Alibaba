package com.spiritmark.service.impl;

import com.spiritmark.dao.OrderDao;
import com.spiritmark.domain.Order;
import com.spiritmark.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    private OrderDao orderDao;

    @Override
    public void createOrder(Order order) {
        orderDao.save(order);
    }


}
